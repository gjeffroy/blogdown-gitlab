[[_TOC_]]


# Page aide
> **Page d'aide en cours de construction**: Certaines informations peuvent être incomplètes ou erronées !


## Définition des EnR

<b> Solaire photovoltaïque</b><br>
Les technologies photovoltaïques (PV) reposent sur des cellules qui transforment le rayonnement solaire en courant électrique continu. Les installations de moins de 36kW ne sont pas prises en compte.<br>

<b> Éolien terrestre </b><br>
Une éolienne est un dispositif qui permet de convertir l’énergie cinétique du vent en énergie mécanique. Seules les éoliennes terrestres sont prises en compte.<br>

<b>Cogénération électrique (biogaz)</b><br>
La filière cogénération électrique biogaz est définie par sa technologie et le type de source servant à produire le biogaz.

On considère les trois sources suivantes :

* Le biogaz issu de station d'épuration (B.EPU)
* Le biogaz de méthanisation (B.MET)
* Le biogaz issu d'installation de stockage de déchets non dangereux

Et les technologies de cogénération suivantes : 

* Cogénération à combustion
* Cogénération à vapeur

La méthanisation est un processus naturel de dégradation biologique de la matière organique dans un milieu sans oxygène due à l’action de multiples microorganismes (bactéries) qui produit du biogaz. Ce biogaz est ensuite introduit dans un module de cogénération produisant ainsi de l’électricité et de la chaleur.


## Définition des mesures 
<b>Nombre d’installations :</b> nombre d’installations présentes sur le territoire au 31/12 de chaque année. Pour le photovoltaïque et l’éolien, le nombre d’installations fait référence au nombre de sites
(parc, centrale) et non à une installation individuelle (un panneau, une éolienne).

<b>Production en GWh :</b> Production d’électricité annuelle au 31/12 de chaque année. Pour l'injection biogaz, on estime la production (quand elle est manquante pour un millésime et une installation donnée) en fonction de la puissance de l'installation et du type d'installation.

<b>Puissance en MW :</b> Puissance référencée sur le territoire au 31/12 de chaque année


## Positionnement des installations sur la carte
<b> Injection Biogaz </b><br>
Les coordonnées géographiques des installations produisant du biogaz injecté dans le réseau sont disponibles directement dans le registre ODRE des points d'injection biométhane.

<b> PV, éolien et cogénération électrique </b><br>
Les coordonnées géographiques de ces installations sont extrapolées à partir de leur IRIS car leur positionnement réel n'est pas disponible dans les données. Ces installations sont placées au niveau du centroïde de l'IRIS correspondant.

## Cas des installations à cheval sur plusieurs départements et/ou régions
Certaines installations sont localisées sur un IRIS à cheval entre plusieurs départements, et parfois régions.
Dans ce cas, on duplique ces installations et on divise leur puissance et production par le nombre de départements / régions. Lorsque l'on sélectionne un département, une installation à cheval sur deux départements sera ainsi présentée avec sa puissance et sa production divisée par deux.

## Données sources 
### source des données

<section class="fr-accordion" data-fr-js-accordion="true"><h3>
<button class="fr-accordion__btn" aria-controls="accordion-114" aria-expanded="false" data-fr-js-collapse-button="true">
Registre ODRE des installations électriques
</button>
</h3>
<div id="accordion-114" class="fr-collapse" data-fr-js-collapse="true" style="--collapse: -144px;"><div class="fr-callout__text ">


<p>                    
<b>Nom :</b> 
<a href="https://odre.opendatasoft.com/explore/dataset/registre-national-installation-production-stockage-electricite-agrege/information/?disjunctive.epci&disjunctive.departement&disjunctive.region&disjunctive.filiere&disjunctive.combustible&disjunctive.combustiblessecondaires&disjunctive.technologie&disjunctive.regime&disjunctive.gestionnaire&refine.region=Grand+Est&refine.departement=Marne&refine.epci=CC+de+Vitry,+Champagne+et+Der&refine.filiere=Eolien&sort=puismaxinstallee" target="_blank">Registre national des installations de production et de stockage d'électricité</a><br>
<b>Version :</b> un set de donnée par an choisi au 31/12 de chaque année entre 2017 jusqu’à 2022<br>
<b>Filière :</b> cogénération électrique biogaz, PV et Eolien <br>
<b>Granularité :</b> installation<br>
<b>Télécharger les données sources : </b><br>

[Registre du 31-12-2017](https://opendata.reseaux-energies.fr/explore/dataset/registre-national-installation-production-stockage-electricite-agrege-311217/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true)
<br>
[Registre du 31-12-2018](https://opendata.reseaux-energies.fr/explore/dataset/registre-national-installation-production-stockage-electricite-agrege-311218/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true)
<br>
[Registre du 31-12-2019](https://opendata.reseaux-energies.fr/explore/dataset/registre-national-installation-production-stockage-electricite-agrege-311219/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true)
<br>
[Registre du 31-12-2020](https://opendata.reseaux-energies.fr/explore/dataset/registre-national-installation-production-stockage-electricite-agrege-311220/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true)
<br>
[Registre du 31-12-2021](https://opendata.reseaux-energies.fr/explore/dataset/registre-national-installation-production-stockage-electricite-agrege-311221/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true)
<br>
[Registre du 31-12-2022](https://opendata.reseaux-energies.fr/explore/dataset/registre-national-installation-production-stockage-electricite-agrege-311222/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true)
</p>
</div></div>
</section>


<section class="fr-accordion" data-fr-js-accordion="true"><h3>
<button class="fr-accordion__btn" aria-controls="accordion-115" aria-expanded="false" data-fr-js-collapse-button="true">
Registre ODRE des installations biométhane
</button>
</h3>
<div id="accordion-115" class="fr-collapse" data-fr-js-collapse="true" style="--collapse: -144px;"><div class="fr-callout__text ">


<p>                    
<b>Nom :</b> 
<a href="https://odre.opendatasoft.com/explore/dataset/production-annuelle-de-biomethane-par-site-raccorde-au-reseau-de-transport-et-de/information/?disjunctive.site&disjunctive.id_unique_projet&disjunctive.nom_du_site&disjunctive.nom_epci&disjunctive.region" target="_blank">Production Annuelle de Biométhane par site raccordé au réseau de transport et de distribution</a><br>
<b>Version :</b> set de donnée le plus récent filtré entre 2017 et 2022<br>
<b>Filière :</b> injection biomethane <br>
<b>Granularité :</b> installation <br>
<b>Télécharger les données sources :</b> <br>
[Version la plus récente](https://opendata.reseaux-energies.fr/explore/dataset/production-annuelle-de-biomethane-par-site-raccorde-au-reseau-de-transport-et-de/download/?format=geojson&timezone=Europe/Berlin&lang=fr)
</p>

<p>
<b>Nom :</b> 
<a href="https://odre.opendatasoft.com/explore/dataset/points-dinjection-de-biomethane-en-france/export/?flg=fr-fr&disjunctive.site&disjunctive.nom_epci&disjunctive.departement&disjunctive.region&disjunctive.grx_demandeur&disjunctive.type_de_reseau&disjunctive.ndeg_de_pitd_pitp&disjunctive.augmentation_prevue" target="_blank">Points d'injection de Biométhane en France en service</a><br>
<b>Version :</b> set de donnée le plus récent filtré entre 2017 et 2022<br>
<b>Filière :</b> injection biomethane <br>
<b>Granularité :</b> installation <br>
<b>Télécharger les données sources :</b> <br>
[Version la plus récente](https://opendata.reseaux-energies.fr/explore/dataset/points-dinjection-de-biomethane-en-france/download/?format=geojson&timezone=Europe/Berlin&lang=fr)
</p>

</div></div>
</section>


<section class="fr-accordion" data-fr-js-accordion="true"><h3>
<button class="fr-accordion__btn" aria-controls="accordion-116" aria-expanded="false" data-fr-js-collapse-button="true">
Tableaux de bord du SDES
</button>
</h3>
<div id="accordion-116" class="fr-collapse" data-fr-js-collapse="true" style="--collapse: -144px;"><div class="fr-callout__text ">


<p>                    
<b>Nom :</b> 
<a href="https://www.statistiques.developpement-durable.gouv.fr/donnees-regionales-et-locales-0?rubrique=23" target="_blank">Données régionales et locales</a><br>
<b>Version :</b> au 31/12/2022 filtré entre 2017 à 2022 <br>
<b>Filière :</b> Eolien et solaire photovoltaïque <br>
<b>Granularité :</b> région et département <br>
<b>Télécharger les données sources :</b> 
<br>
[Données Photovoltaïque au 31-12-2022](https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2023-02/2022_t4_tb_dpt_installations_solaires_photovoltaiques.xlsx)
<br>
[Données Eolien au 31-12-2022](https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2023-02/2022_t4_tb_dpt_installations_eoliennes.xlsx)

</p>
</div></div>
</section>



<section class="fr-accordion" data-fr-js-accordion="true"><h3>
<button class="fr-accordion__btn" aria-controls="accordion-117" aria-expanded="false" data-fr-js-collapse-button="true">
Objetifs SRADDET
</button>
</h3>
<div id="accordion-117" class="fr-collapse" data-fr-js-collapse="true" style="--collapse: -144px;"><div class="fr-callout__text ">

<p>                    
<b>Filière :</b> production biogaz, éolien et solaire photovoltaïque<br>
<b>Granularité :</b> région <br>
<b>Années des objectifs :</b> 4 jalons entre 2012 et 2050, variable en fonction de la région<br>
<b>Note :</b> Les objectifs SRADDET sont disponibles uniquement à l’échelle régionale. Ils sont fournis directement par la région concernée. Il n'y a pas d’objectif SRADDET pour la cogénération électrique biogaz<br>
</p>
</div></div>
</section>

### Utilisation des données


- <b> Nombre d'installations et Puissance en GWh </b><br>

Le nombre d'installation et la puissance en GWh remontent uniquement des registres ODRE (biogaz ou elec en fonction).
Les données sont agregées en sommant les installations suivant le niveau d'agrégation souhaitée.
Ci-dessous les sources en fonction des cas de figure :

| Filière                         | France              | Région              | Département          | EPCI                 |
| :------------------------------ |:-------------------:|:-------------------:|:--------------------:| --------------------:|
| <b>Injection biogaz         </b>| Registre ODRE biométhane| Registre ODRE biométhane| Registre ODRE biométhane | Registre ODRE biométhane |
| <b>Cogénération électrique </b> | Registre ODRE Elec  | Registre ODRE Elec  | Registre ODRE Elec   | Registre ODRE Elec   |
| <b>Eolien                  </b> | Registre ODRE Elec  | Registre ODRE Elec  | Registre ODRE Elec   | Registre ODRE Elec   |
| <b>Photovoltaïque           </b>| Registre ODRE Elec  | Registre ODRE Elec  | Registre ODRE Elec   | Registre ODRE Elec   |
<br>

- <b> Puissance en MW </b><br>

La puissance en MW est remontée du SDES quand elle est disponible, sinon les valeurs sont agregées en sommant les installations suivant le niveau d'agrégation souhaitée.
Ci dessous les sources en fonction des cas de figure :

| Filière                         | France              | Région              | Département          | EPCI                 |
| :------------------------------ |:-------------------:|:-------------------:|:--------------------:| --------------------:|
| <b>Injection biogaz         </b>| Registre ODRE biométhane| Registre ODRE biométhane| Registre ODRE biométhane | Registre ODRE biométhane |
| <b>Cogénération électrique </b> | Registre ODRE Elec  | Registre ODRE Elec  | Registre ODRE Elec   | Registre ODRE Elec   |
| <b>Eolien                  </b> | Registre ODRE Elec  | SDES                | SDES                 | Registre ODRE Elec   |
| <b>Photovoltaïque           </b>| Registre ODRE Elec  | SDES                | SDES                 | Registre ODRE Elec   |

<br><br>


## Crédits et mentions légales

<b>Contexte</b><br>
Cette application a été développée par le Pôle Connaissance EnR, pôle national piloté par la DREAL Grand Est.<br>
Les pôles “Connaissance” thématiques sont coordonnés par la Mission Connaissance du MTE/CGDD/Ecolab.<br>
Directeur de la mission : Sébastien Pons, sebastien.pons@developpement-durable.gouv.fr<br>
Adjoint au directeur de mission : Marina Ribeiro, marina.ribeiro@developpement-durable.gouv.fr<br>

<b>Service gestionnaire</b><br>
Direction Régionale de l’Environnement de l’Aménagement et du Logement (DREAL) du Grand Est<br>
Service Connaissance et Développement Durable - Pôle Connaissance EnR<br>
2 rue Augustin Fresnel - CS 95038<br>
57071 METZ Cedex 03<br>
Tél : 03 87 62 81 00<br>
Courriel : scdd.dreal-grand-est@developpement-durable.gouv.fr<br>

<b>Directeur de la publication</b><br>
Hervé Vanlaer, directeur régional de l’environnement, de l’aménagement et du logement du Grand Est<br>

<b>Conception et réalisation</b><br>
D’après l’outil TEO de la DREAL Pays de Loire
<a href="http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/enr_reseaux_teo/" target="_blank">http://apps.datalab.pays-de-la-loire.developpement-durable.gouv.fr/enr_reseaux_teo/</a><br>

* Maquettage et prototype : Accenture/Octo, suivi par CGDD/Ecolab (Bruno Lenzi)
* Développement : Accenture/Octo, puis <a href="https://www.malt.fr/profile/guillaumejeffroy" target="_blank">Guillaume Jeffroy</a><br>
* Chef de projet : DREAL Grand Est (François Mathonnet, aidé d’Olivier Guérin)<br>
* Equipe projet : DREAL Grand Est (SCDD et STECCLA), DDT 08, 51, 57 et 67, DRAAF, IGN, DGEC

<b>Hébergement</b><br>
Plateforme shinyapps du ministère de la Transition écologique<br> (https://ssm-ecologie.shinyapps.io)<br>

<b>Droit d’auteur et licence</b><br>
Tous les contenus présents sur le site de la direction régionale de l’Environnement, de l’Aménagement et du Logement du Grand Est sont couverts par le droit d’auteur. Toute reprise est dès lors conditionnée à l’accord de l’auteur en vertu de l’article L.122-4 du Code de la Propriété Intellectuelle.

Toutes les informations liées à cette application (données et textes) sont publiées sous licence ouverte/open licence v2 (dite licence Etalab) : quiconque est libre de réutiliser ces informations, sous réserve notamment, d'en mentionner la filiation.

Tous les scripts source de l'application sont disponibles sous licence GPL-v3.

<b>Code source</b><br>
Le code de l’application est disponible sur le GitLab de la DREAL :<br>
<a href="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/outilsenr/outilenr_etatdeslieux" target="_blank">https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/outilsenr/outilenr_etatdeslieux</a><br>


<b>Établir un lien</b><br>
Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées par le ministère de la Transition écologique et le ministère de la Cohésion des Territoires.
L’autorisation de mise en place d’un lien est valable pour tout support, à l’exception de ceux diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure porter atteinte à la sensibilité du plus grand nombre.

<b>Usage</b><br>
Les utilisateurs sont responsables des interrogations qu’ils formulent ainsi que de l’interprétation et de l’utilisation qu’ils font des résultats. Il leur appartient d’en faire un usage conforme aux réglementations en vigueur et aux recommandations de la CNIL lorsque des données ont un caractère nominatif (loi n° 78.17 du 6 janvier 1978, relative à l’informatique, aux fichiers et aux libertés dites loi informatique et libertés).<br>

Il appartient à l’utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par d’éventuels virus circulant sur le réseau Internet. De manière générale, la Direction Régionale de l’Environnement de l’Aménagement et du Logement décline toute responsabilité à un éventuel dommage survenu pendant la consultation du présent site. Les messages que vous pouvez nous adresser transitant par un réseau ouvert de télécommunications, nous ne pouvons assurer leur confidentialité.<br>

<b>Contact</b><br>
Pour tout renseignement, suggestion sur cette application, merci de laisser un message à scdd.dreal-grand-est@developpement-durable.gouv.fr

<b>Désactivation du suivi</b><br>
<iframe style="border: 0; height: 200px; width: 600px;" src="https://audience-sites.din.developpement-durable.gouv.fr/optinout.php?language=fr&backgroundColor=ffffff&fontColor=484848&fontSize=0.7em&fontFamily=Arial%2C%20Verdana%2C%20Geneva%2C%20Helvetica%2C%20sans-serif"></iframe>
